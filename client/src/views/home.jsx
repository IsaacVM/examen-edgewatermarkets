import React, { Component, Fragment } from 'react';
import Header from '../components/header';
import Loading from '../assets/200.gif';

class App extends Component {
  constructor() {
    super();
    this.state = {
      index : '',
      res: '',
      time: '',
      load: false,
    };
  }
  render(){
    const { index, res, load, time } = this.state;
    return (
      <Fragment>
        <Header/>
        <div className="home">
          <div>
            <p>
              1) Encuentra de forma optima el indice de un numero dentro de un array ordenado.
            </p>
            <p>
              array [1, 2, 3, 6, 8, 10, 11, 13]
            </p>
            <input ref='arrIndex' type='text'/>
            <button onClick={ this.valorArray }>
              Buscar Indice
            </button>
            <p>
              { index }
            </p>
          </div>
          <div>
            <p>
              2) ReactJS y NodeJS
              Problema de Fibonacci:
              En matemáticas, la sucesión o serie de Fibonacci hace referencia a la secuencia ordenada de números descrita por Leonardo de Pisa, matemático italiano del siglo XIII:
              0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144,…
            </p>
           <input ref='fib' type='text'/>
            <button onClick={ this.llamada }>
              Buscar Indice
            </button>
            <p>
              { !load && res }
            </p>
            <p>
              { !load && time }
            </p>
            { load && <img src={ Loading } alt='loading'/> }
          </div>
        </div>
      </Fragment>
    );
  }

  valorArray = () => {
    const arr = [1, 2, 3, 6, 8, 10, 11, 13];
    const busqueda = this.refs['arrIndex'].value;
    let res = undefined;
    
    arr.forEach( (element, i) => {
      if ( element == busqueda ) {
        res = `El número ${ busqueda } esta en la posición ${ i }`;
      }
    });

    this.setState({
      index: res ? res : `No se encuentro (${ busqueda }) en el arreglo`,
    });
  }

  llamada = () => {
    const numero = parseInt(this.refs['fib'].value);

    this.setState({
      load: true,
    });

    fetch('http://localhost:9000/fib', {
      method: 'POST',
      body: JSON.stringify({ 'n' : numero}),
      headers: {
        'Access-Control-Allow-Origin':'*',
        'Content-Type': 'application/json',
      },
    }).then(res => { 
      return res.json()
    }).then(res => {
      this.setState({
        res: !isNaN(numero) ? `Indice ${ numero }: numero fib =${ res.responce }`: res.responce,
        time: `Tardo ${ res.time }`,
        load: false,
      });
    }).catch(error => {
      // console.log('E un error : ', error);
      console.log(error);
    });
  }
}

export default App;
