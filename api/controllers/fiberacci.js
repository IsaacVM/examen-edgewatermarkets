
module.exports.resFibb = (req, res, next) => {
  const number = req.body;

  let start = new Date()
  let hrstart = process.hrtime()
  let time = 0;

  if (number.n === null) {
    let hrend = process.hrtime(hrstart);
    time = `${ hrend[1] / 1000000 } ms`;
    return res.status(500).json({
      'responce':'El dato enviado no es un número',
      'time' : time,
    });
  } else {
    function e(n) {
      let end = new Date() - start;
      let hrend = process.hrtime(hrstart);
      time = `${ hrend[1] / 1000000 } ms`;

      // console.info('Execution time: %dms', end)
      // console.info('Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1000000)

      if (n === 0) {
        return 0;
      } else if (n === 1) {
        return 1;
      } else if (n >1) {
        return e(n-1) + e(n-2);
      }
    }

    return res.status(200).json({
      'responce' : e(number.n),
      'time' : time,
    });
  }
}